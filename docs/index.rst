
PyQTermWidget
=========================================

The `PyQTermWidget` project is a console/terminal widget for python3/PyQt5 and let's you
embed a shell into an application.

- The vt100 terminal emulation code is based on AjaxTerm and WebShell.
- All code is distributed under the General Public License 2.

.. note::
   This project is a fork of the original PyQt4 see below

Contents:

.. toctree::
   :maxdepth: 2
   
   usage
   pyqterm/index.rst
   todo

.. image:: screenshot2.png


How to get it
-------------


clone the git repository

 git clone https://gitlab.com/pyqtx/pyqtermwidget.git



Fork Note:
=================

.. note::

  This is a fork of the original project at https://bitbucket.org/henning/pyqtermwidget details below


The latest snapshot is available at:
  https://bitbucket.org/henning/pyqtermwidget/get/tip.tar.bz2

To clone the Mercurial repository do:
  hg clone https://bitbucket.org/henning/pyqtermwidget

Or just got to the BitBucket overview page: https://bitbucket.org/henning/pyqtermwidget



The Python Packaging Index (PyPI) also contains releases:  http://pypi.python.org/pypi/pyqterm

The generated HTML-documentation is hosted at http://pyqtermwidget.rtfd.org/
(PDF: http://media.readthedocs.org/pdf/pyqtermwidget/latest/pyqtermwidget.pdf).
   
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

