PyQTermWidget
=============

This is console widget for python3/PyQt5 and let's you embed a shell into your application. 

Browse the API and examples at https://pyqtx.gitlab.io/pyqtermwidget/

```python

from PyQt5.QtGui import QApplication
from pyqterm import TerminalWidget


if __name__ == "__main__":
  app = QApplication(sys.argv)

  win = TerminalWidget()
  win.resize(800, 600)
  win.show()

  app.exec_()



Look at [pyqterm-app.py](pyqterm-app.py) for a more complete example with a tabbed terminal application.


![screenshot](docs/screenshot2.png)
```